<!DOCTYPE html>
<html>
<head>
<!-- Font Awesome Icon Library -->

<style>
.checked {
  color: orange;
}

</style>
<script>
  window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
  theme: "light2", // "light1", "light2", "dark1", "dark2"
  exportEnabled: true,
  animationEnabled: true,
  title: {
    text: "Rating spitale"
  },
  data: [{
    type: "pie",
    startAngle: 25,
    toolTipContent: "<b>{label}</b>: {y}%",
    showInLegend: "true",
    legendText: "{label}",
    indexLabelFontSize: 16,
    indexLabel: "{label} - {y}%",
    dataPoints: [
      { y: 31.08, label: "Spitalul Universitar C.F.Cluj" },
      { y: 27.34, label: "Spitalul Clinic Judetean de Urgenta Cluj-Napoca" },
      { y: 15.64, label: "Spitalul Clinic de Recuperare" },
      { y: 14.07, label: "Institutul Oncologic Prof. Dr. Ion Chiricuta" },
      { y: 10.66, label: "Institutul Inimii Niculae Stancioiu" }
    ]
  }]
});
chart.render();

}
</script>
</head>
<body>

	<div class="topnav">
  <a href="profil.php">Profil</a>
  <a href="index.php">Acasa</a>

	<br><h1>Adaugare comentarii si rating medici</h1></br>

	<h2>Scrie un comentariu:</h2>

	<div class="comment">
<textarea id="title" type="text "rows="10" cols="100" onkeyup="Allow()" placeholder="write a comment..."></textarea>
<br><input type="submit" value="Post" onclick="insert()" style="width:150px;" /></form></br>
</div>
<div id="display"></div>
<script type="text/javascript">
var titles = [];
var titleInput = document.getElementById("title");
var messageBox = document.getElementById("display");
function Allow()
{
if (!user.title.value.match(/[a-zA-Z]$/) && user.title.value !="")
{
user.title.value="";
alert("Please Enter only alphabets");
}
window.location.reload()
}
function insert () {
titles.push(titleInput.value);
clearAndShow();
}
function clearAndShow ()
{
titleInput.value = "";
messageBox.innerHTML = "";
messageBox.innerHTML += " " + titles.join("<br/> ") + "<br/>";
}
</script>

<br><h2>Evaluare medici</h2></br>

<div class="rate">
    <input type="radio" id="star5" name="rate" value="5" />
    <label for="star5" title="text">5 stars</label>
    <input type="radio" id="star4" name="rate" value="4" />
    <label for="star4" title="text">4 stars</label>
    <input type="radio" id="star3" name="rate" value="3" />
    <label for="star3" title="text">3 stars</label>
    <input type="radio" id="star2" name="rate" value="2" />
    <label for="star2" title="text">2 stars</label>
    <input type="radio" id="star1" name="rate" value="1" />
    <label for="star1" title="text">1 star</label>
  </div>
  <br></br>
  <p><input type="submit" value="Submit" onclick="insert()" style="width:150px;" /></form></p>
  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    <style type="text/css">
    .wrapper{
      width: 350px;
      padding: 20px;
    }
    
    li a:hover {
      background-color: transparent;
    }
    body {
       background-image: url("Hospital1.jpg");
    }

    input[type=text], input[type=password] {
      width: 30%;
      padding: 12px 20px;
      margin: 8px 0;
      display: block;
      float: center;
      border: 1px solid black;
      box-sizing: border-box;
    }
    .imgcontainer {
      text-align: center;
      margin: 24px 0 12px 0;
    }

    img.avatar {
      width: 15%;
      border-radius: 50%;
    }

    .container {
      padding: 16px;
    }
    .topnav {
      overflow: hidden;
      background-color: transparent;
    }

    /* Style the topnav links */
    .topnav a {
      float: right;
      display: block;
      color: black;
      text-align: center;
      font-weight: bold;
      padding: 30px 30px;
      text-decoration: none;
    }

    /* Change color on hover */
    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }
    h2, p, label{
        color: black;
    }
    }
    *{
    margin: 0;
    padding: 0;
}
.rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    position:absolute;
    top:-9999px;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
    </style>
</body>
</html>