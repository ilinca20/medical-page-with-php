<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// database connection will be here
include_once 'C:\xampp\htdocs\Web\api\configdatabase.php';
include_once 'C:\xampp\htdocs\Web\api\user_details.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$user_details = new User_details($db);
 
// read products will be here

// query products
$stmt = $user_details->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $user_details_arr=array();
    $user_details_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
       $user_details_item=array(
            "userID" =>  $userID,
            "nume" => $nume,
            "afectiune"=> html_entity_decode($afectiune),
            "imgProfil"=> $imgProfil,
	"loginID" => $loginID
        );

        array_push($user_details_arr["records"], $user_details_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($user_details_arr);
}
 
// no products found will be here

else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}