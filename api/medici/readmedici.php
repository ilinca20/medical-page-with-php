<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// database connection will be here
include_once 'C:\xampp\htdocs\Web\api\config\database.php';
include_once 'C:\xampp\htdocs\Web\api\objects\medici.php';

$idSpital = isset($_GET['idSpital']) ? $_GET['idSpital'] : die();

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$medici = new Medici($db);
 
// read products will be here

// query products
$stmt = $medici->read($idSpital);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $medici_arr=array();
    $medici_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $medici_item=array(
            "id Medic" => $idMedic,
            "nume" => $nume,
            "specializare" => html_entity_decode($specializare),
            "anAbsolv" => $anAbsolv,
            "idSpital" => $idSpital,
            "pozaMed" => $pozaMed
        );
 
        array_push($medici_arr["records"], $medici_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($medici_arr);
}
 
// no products found will be here

else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}