<?php
class Spitale{
 
    // database connection and table name
    private $conn;
    private $table_name = "spitale";
 
    // object properties
    public $idSpital;
	public $nume;
	public $tip;
	public $locatie; 
	public $sigla;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
// read products
function read(){
 
    // select all query
    $query = "SELECT * FROM spitale";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
}
}
