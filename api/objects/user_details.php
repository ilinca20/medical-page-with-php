<?php
class User_details{
 
    // database connection and table name
    private $conn;
    private $table_name = "user_details";
 
    // object properties
    public $userID;
	public $nume;
	public $afectiune;
	public $imgProfil; 
	public $loginID;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
// read products
function read(){
 
    // select all query
    $query = "SELECT * FROM user_details";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
}
}
