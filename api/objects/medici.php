<?php
class Medici{
 
    // database connection and table name
    private $conn;
    private $table_name = "medici";
 
    // object properties
    	public $idMedic;
	public $nume;
	public $specializare; 
	public $anAbsolv;
	public $idSpital;
	public $pozaMed;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }
// read products
function read($idSpital){

    if($idSpital == 0) {
    // select all query
    $query = "SELECT * FROM medici";
    } else {
    $query = "SELECT * FROM medici WHERE idSpital = '$idSpital'";
    }
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
}
}
