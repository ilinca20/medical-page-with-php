<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// database connection will be here
include_once 'C:\xampp\htdocs\Web\api\config\database.php';
include_once 'C:\xampp\htdocs\Web\api\objects\spitale.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$spitale = new Spitale($db);
 
// read products will be here

// query products
$stmt = $spitale->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $spitale_arr=array();
    $spitale_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
       $spitale_item=array(
            "idSpitale" =>  $idSpital,
            "nume" => $nume,
            "tip"=> html_entity_decode($tip),
            "locatie"=> $locatie,
		"sigla" => $sigla
        );

        array_push($spitale_arr["records"], $spitale_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($spitale_arr);
}
 
// no products found will be here

else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No products found.")
    );
}